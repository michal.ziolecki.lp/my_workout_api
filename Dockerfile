FROM python:3.10
WORKDIR /app/
RUN adduser ops && chown -R ops /app/
COPY poetry.lock /app/
COPY pyproject.toml /app/
RUN pip install poetry
RUN poetry config virtualenvs.create false
RUN poetry install
COPY . /app/
EXPOSE 8000
USER ops
HEALTHCHECK --timeout=5s --retries=2 --interval=30s CMD curl --fail http://localhost:8000/ping || exit 1
ENTRYPOINT ["uvicorn", "main:app", "--host", "0.0.0.0", "--port", "8000"]
