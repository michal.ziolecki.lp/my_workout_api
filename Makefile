style:
	@echo "checking code style with pre-commit... "
	pre-commit run --all-files

install:
	@echo "installing virtual environment... "
	poetry install

build:
	docker build . -t workout_api

run:
	docker run -p 8000:8000 workout_api

clean:
	@echo "cleaning... "
	find . -type f -iname "*.pyc" -or -iname "*.pyo" -delete
	find . -type d -name __pycache__ -exec rm -r {} +
	rm -rf build dist
