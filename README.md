# my_workout_api

#### - API for android application.
#### - API is prepared in order to store data related to user workout scores.

## Default dependencies package manager

    poetry --help

## Pre-commit quality checker

    pre-commit run --all-files

## Suggested venv tool

    pyenv

## Build project

    make build

##
